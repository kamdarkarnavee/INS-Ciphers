text = input("Enter plain text that you want to encrypt : ")
key = int(input("Enter key : "))%26


def encryption(text, key):	
	encrypted_text = ''
	for i in text:	
		i = ord(i)		
		if((i >= 65 and i <= 90) or (i >= 97 and i <= 122)):
			encrypt = (i + key)
			if( ((encrypt > 90) and (encrypt < 97)) or (encrypt > 122) ):
				encrypt = (i + key - 26)
		else:
			print("Enter plain text only - no spaces, special symbols or numbers allowed")	
			exit()
		encrypted_text += chr(encrypt)
	return encrypted_text

def decryption(text, key):	
	decrypted_text = ''
	for i in text:	
		i = ord(i)		
		if((i >= 65 and i <= 90) or (i >= 97 and i <= 122)):
			decrypt = (i - key)
			if( ((decrypt > 90) and (decrypt < 97)) or (decrypt < 65) ):
				decrypt = (i - key + 26)					
		decrypted_text += chr(decrypt)
	return decrypted_text


encrypted_text = encryption(text, key)
decrypted_text = decryption(encrypted_text, key)
print("Encrypted text :", encrypted_text)
print("Decrypted text :", decrypted_text)