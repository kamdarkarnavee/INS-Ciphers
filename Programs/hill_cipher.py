import numpy as np
import math
# plain_text = "short example"
# key = "hill"
# plain_text = "retreat now"
# key = "backup"
# plain_text = "we are safe"
# key = "alphabet"

global ki_matrix
global rem
plain_text = input("Enter plain text: ")
key = input("Enter key: ")
sqrt_keylen = int(math.ceil(math.sqrt(len(key))))
diff = (sqrt_keylen * sqrt_keylen) - len(key)
size = sqrt_keylen
for i in range(diff):
    key += chr(97+i)

key_list = [ord(i)-97 for i in key]
key_matrix = np.matrix(key_list).reshape((size, size))

k_determinant = int(round(np.linalg.det(key_matrix)))
if k_determinant == 0:
    print('Please choose some other key')
    exit(0)
k_determinant %= 26
# print("Key determinant", k_determinant)
# print("Key matrix :\n", key_matrix)
length_plaintext = len(plain_text)

#plain text to matrix
text_list = [ord(i)-97 for i in plain_text if i != " "]
rem = 0
if len(text_list) % size != 0:
    quotient = int(math.ceil(len(text_list) / size))
    rem = quotient * size - len(text_list)
    for i in range(rem):
        text_list.append(ord('x') - 97)
no_of_rows = int(len(text_list) / size)
text_matrix = np.matrix(text_list).reshape((no_of_rows, size))
# print("Plain text matrix:\n", text_matrix)

# Encryption
for row in range(len(text_matrix)):
    # CT = (Key * PT) mod 26
    cipher = key_matrix * text_matrix[row].reshape((size, 1)) % 26
    text_matrix[row] = cipher.reshape((1, size))


ciphered_matrix = text_matrix
cipher_text = np.array(text_matrix).ravel()
ct = ""
for i in cipher_text:
    ct += chr(i+97)
print("Encrypted text:", ct)

#finding multiplicative inverse
multiplicative_inverse = k_determinant
for i in range(26):
    if k_determinant * i % 26 == 1:
        multiplicative_inverse = i
        break
#print("multiplicative inverse :", multiplicative_inverse)

key_inverse = []
if size == 2:
    key_inverse.append(key_matrix.item(1, 1))
    key_inverse.append(-(key_matrix.item(0, 1)))
    key_inverse.append(-(key_matrix.item(1, 0)))
    key_inverse.append((key_matrix.item(0, 0)))
    ki_mat = np.matrix(key_inverse).reshape((size, size))
    ki_matrix = np.matrix(ki_mat) * multiplicative_inverse % 26

#print(key_inverse)
if size == 3:
    for row in range(size):
        for col in range(size):
            key_inverse.append(key_matrix.item((row + 1) % size, (col + 1) % size) * key_matrix.item((row + 2) % size, (col + 2) % size)
                                    - key_matrix.item((row + 1) % size, (col + 2) % size) * key_matrix.item((row + 2) % size,
                                                                                                        (col + 1) % size))
    ki_mat = np.matrix(key_inverse).reshape((size, size))
    ki_matrix = np.matrix.getH(ki_mat) * multiplicative_inverse % 26

decrypted_matrix = np.matrix(cipher_text).reshape((no_of_rows, size))

for row in range(len(ciphered_matrix)):
    # Pt = (key_inverse * CT) mod 26
    decryption = ki_matrix * ciphered_matrix[row].reshape((size, 1)) % 26
    decrypted_matrix[row] = decryption.reshape((1, size))

# print(decrypted_matrix + 97)
decrypted_text = np.array(decrypted_matrix).ravel()
ans = ""
for i in range(len(text_list) - rem):
    ans += chr(decrypted_text[i] + 97)
print("Decrypted text:", ans)