import numpy as np
key = input("Set key : ").replace(" ", "").lower()
plain_text = input("Enter plain text : ").replace(" ", "").lower()
ls = []
row = []
def row_matched(element, t):
	j = 0
	encrypted_text = ""
	while j < len(row):
		if element == row[j]:
			if t == "encrypt":
				encrypted_text += row[(j+1)%5]
			elif t == "decrypt":
				if j != 0:
					encrypted_text += row[(j-1)]
				else:
					encrypted_text += row[4]
			break
		j += 1
	return encrypted_text

def getColIndex(element):
	j = 0
	index = 0
	while j < len(row):
		if element == row[j]:
			index = j
			break
		j += 1
	return index

#plain text digraph
def digraph(text):
	i=0
	#list to store plain text
	pt = list()
	if len(text) % 2 != 0:
		text += 'y'

	while i != len(text):
		pt.append([text[i], text[i+1]])
		i += 2
	print(pt)
	return pt

#code to construct key matrix
for i in key:
	if i not in ls:
		ls.append(i)

for c in range(97,123):
	if len(ls) == 25:
		break;
	if chr(c) not in ls:
		if (chr(c) == 'i' or chr(c) == 'j'):
			#solves the problem of i and j if present in plain text
			if chr(c) in plain_text:
				ls.append(chr(c))
			if(('i' in ls) or ('j' in ls)):
				#print('i or j is present')
				continue
		ls.append(chr(c))

matrix = np.array(ls).reshape((5,5))
print(matrix)


def text_conversion(pt, t):
	encrypted_text = ""
	for element in pt:
		i1, i2, j1, j2 = 0, 0, 0, 0
		#used flags in order to know whether the elements were in the same row or different
		#as we want to the execute the code of if statement only if the elements aren't in same row
		flag1, flag2 = False, False
		global row
		for index, row in enumerate(matrix):
			if element[0] in row and element[1] in row:
				encrypted_text += row_matched(element[0], t) + row_matched(element[1], t)
				break
			else:
				if element[0] in row:
					i1 = index
					j1 = getColIndex(element[0])
					flag1 = True

				if element[1] in row:
					i2 = index
					j2 = getColIndex(element[1])
					flag2 = True

		if(flag1 and flag2):
			if j1 == j2:
				if t == "encrypt":
					encrypted_text += matrix[(i1+1)%5, j1] + matrix[(i2+1)%5, j2]
				elif t == "decrypt":
					if i1 == 0:
						encrypted_text += matrix[4, j1] + matrix[(i2-1), j2]
					elif i2 == 0:
						encrypted_text += matrix[(i1-1), j1] + matrix[4, j2]
					else:
						encrypted_text += matrix[(i1-1), j1] + matrix[(i2-1), j2]
			else:
				encrypted_text += matrix[i1,j2] + matrix[i2,j1]
	return encrypted_text

pt = digraph(plain_text)
encrypted_text = text_conversion(pt, "encrypt")
print("Encrypted text : ", encrypted_text)

dt = digraph(encrypted_text)
decrypted_text = text_conversion(dt, "decrypt")
print("Decrpted text : ", decrypted_text)