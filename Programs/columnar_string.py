import math
#plain_text = "potatoes are in the nightshade family as well".lower()
plain_text = input("Enter plain text : ").lower()
plain_arr = plain_text.split()
'''
k = input("Enter key : ")
key = list(map(int, k.split(" ")))
OR 
key = [int(x) for x in input("Enter key : ").split(" ")]
'''
key = [int(x) for x in input("Enter key : ").split(" ")]    #0 is not a valid input for key
#key = [4, 2, 5, 1, 6, 3]
text = "".join(plain_text.split(' '))
no_rows = math.ceil(len(text)/len(key))
if no_rows * len(key) != len(text):
    x = (no_rows * len(key)) - (len(text))
    for i in range(x):
        text += 'x'


def out_format(fm, val):
    count = 0
    o_text = ""
    for element in plain_arr:
        o_text += fm[count:count + len(element)]
        count += len(element)
        o_text += " "
    if val:
        o_text += fm[count:]
    return o_text


def encryption(p_text, k):
    k.sort()
    e_text = ""
    for i in range(len(k)):
        index = k.index(k[i])
        while index < len(p_text):
            e_text += p_text[index]
            index += len(k)
    return e_text


def decryption(e_text, k):
    e_text = "".join(e_text.split(' '))
    d_text = ""
    for j in range(no_rows):
        for i in range(len(k)):
            index = (k[i] - 1) * no_rows + j
            d_text += e_text[index]
    return d_text


encrypted_text = out_format(encryption(text, key), True)
print("Encrypted message :", encrypted_text)
decrypted_text = out_format(decryption(encrypted_text, key), False)
print("Original message :", decrypted_text)
