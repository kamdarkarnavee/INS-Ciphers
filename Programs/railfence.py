plain_text = input("Enter plain text : ").lower()
flag = False
len_of_text = len(plain_text)
if len_of_text % 2 != 0:
    flag = True
    plain_text += 'x'


def convert(func, text):
    i = 0
    global flag
    str1 = ""
    str2 = ""
    if func == "encryption":
        while i != len(text):
            str1 += text[i]
            str2 += text[i+1]
            i += 2
        str1 += str2
    elif func == "decryption":
        half_len = int(len(text)/2)
        for i in range(half_len):
            str1 += text[i] + text[i+half_len]
        if flag:
            str1 = str1[:len_of_text]
    return str1


encrypted_text = convert("encryption", plain_text)
print("Encryption:", encrypted_text)
decrypted_text = convert("decryption", encrypted_text)
print("Decryption:", decrypted_text)